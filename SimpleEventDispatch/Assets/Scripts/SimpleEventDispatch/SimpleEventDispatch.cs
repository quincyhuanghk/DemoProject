﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

/// <summary>
/// Simple event dispatch is the singleton.  
/// 
/// use for registering listeners and dispatch the events.
/// 
/// all events DO NOT require return values.
/// 
/// *** it should has a high priority in the execution order ***
/// 
/// Haikun Huang
/// Created on 8/11/2016 
/// </summary>
/// 

    

public class SimpleEventDispatch : MonoBehaviour {


    #region customize

    public enum SEDEventTypes
    {
        Demo1,
        Demo2
    }





	#endregion

	#region system function


    // event delegates
    public delegate void EventsDelegateFunc(ISEDData data);
    // customize events, 
    //EventsDelegateFunc eventDemo1, eventDemo2;
    Dictionary<SEDEventTypes, EventsDelegateFunc> eventDict;


    // Initilize Delegate Event Function
    void Initilize_Delegate_Event_Function()
    {
        //eventDemo1 = DefaultEventFunc;
        //eventDemo2 = DefaultEventFunc;
        eventDict = new Dictionary<SEDEventTypes, EventsDelegateFunc>();
    }


    // send message
	/// <summary>
	/// Send the specified type and data.
	/// return No. that how many listenrs listen this event
	/// </summary>
	/// <param name="type">Type.</param>
	/// <param name="data">Data.</param>
    public int Send(SEDEventTypes type, ISEDData data)
    {
        if (eventDict.ContainsKey(type))
        {
            eventDict[type](data);
        }
        else
        {
           
        }
		int number = eventDict [type].GetInvocationList ().Length - 1;
		Debug.Log ("Event [" + type.ToString() + "] has " + number + " listeners.");
		return number;
    }

    // add listener, cunstomize event
    public void AddListener(SEDEventTypes type, EventsDelegateFunc func)
    {
        if (eventDict.ContainsKey(type))
        {
   
        }
        else
        {
            eventDict.Add(type, DefaultEventFunc);

        }

        eventDict[type] += func;
    }

    // remove listener
    public void RemoveListener(SEDEventTypes type, EventsDelegateFunc func)
    {
        if (eventDict.ContainsKey(type))
        {
            eventDict[type] -= func;
        }
        else
        {

        }
    }




    // singleton
    static SimpleEventDispatch singleton;

    void Awake()
    {
        singleton = this;
        Initilize_Delegate_Event_Function();
    }

    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    void Update () {

    }

    // return the signleton
    static public SimpleEventDispatch GetSignleton()
    {
        return singleton;
    }

    // default event function
    public void DefaultEventFunc(ISEDData data)
    {
        
    }

    #endregion
}
