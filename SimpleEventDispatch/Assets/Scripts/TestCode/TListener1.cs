﻿using UnityEngine;
using System.Collections;

public class TListener1 : MonoBehaviour {

    void OnEnable() 
    {
        // register Demo1 event
        SimpleEventDispatch.GetSignleton().AddListener(SimpleEventDispatch.SEDEventTypes.Demo1, Demo1);

        // register Demo2 event
        SimpleEventDispatch.GetSignleton().AddListener(SimpleEventDispatch.SEDEventTypes.Demo2, Demo2);
    }

    void OnDisable() 
    {
        // register Demo1 event
        SimpleEventDispatch.GetSignleton().RemoveListener(SimpleEventDispatch.SEDEventTypes.Demo1, Demo1);

        // register Demo2 event
        SimpleEventDispatch.GetSignleton().RemoveListener(SimpleEventDispatch.SEDEventTypes.Demo2, Demo2);
    }
        

    void Demo1(ISEDData data)
    {
        Debug.Log("event Demo1: "+ name);
    }

    void Demo2(ISEDData data)
    {
        if (data == null)
        {
            Debug.LogError("require an ISEDData");
        }

        var t = (TData)data;

        Debug.Log(name + ", " + t.i);
    }


}
