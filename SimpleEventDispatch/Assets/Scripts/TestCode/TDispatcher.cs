﻿using UnityEngine;
using System.Collections;


public class TDispatcher : MonoBehaviour {

	// Use this for initialization
	void Start () {
        DispatchDemo();
	}
	
    // demo1 display the gameobject's name
    void DispatchDemo()
    {
        SimpleEventDispatch.GetSignleton().Send(SimpleEventDispatch.SEDEventTypes.Demo1, null );

        TData data = new TData();
        data.i = 200;

        SimpleEventDispatch.GetSignleton().Send(SimpleEventDispatch.SEDEventTypes.Demo2, data);
    }

	// Update is called once per frame
	void Update () {
        DispatchDemo();
	}





}
