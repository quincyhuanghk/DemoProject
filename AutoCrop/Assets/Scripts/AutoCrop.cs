﻿using UnityEngine;
using System.Collections;
using System.IO;


// auto crop screen-shot
public class AutoCrop : MonoBehaviour {

	// capture a screen to .png, fileName does NOT require the file type (.*)
	static public IEnumerator CaptureToPng (string fileName)
	{

		Application.CaptureScreenshot (fileName + ".png");
		// if faild, need to increase the waitting time. 
		yield return new WaitForSeconds (1f);

		WWW w = new WWW ("file://" + fileName + ".png");
		yield return w;

		Texture2D tex = w.texture;


		tex = Trim_Tex (tex);

		// Encode texture into PNG
		byte[] bytes =  tex.EncodeToPNG();

		// For testing purposes, also write to a file in the project folder
		File.WriteAllBytes(fileName + ".png", bytes);

        Debug.Log("Save to: " + fileName);

		yield return null;
	}

    public void Run_AutoCrop(string fileName)
    {
        StartCoroutine(CaptureToPng(fileName));
    }

	// crop texture
	static Texture2D Trim_Tex(Texture2D tex)
	{
		Texture2D result;
		// Background color
		Color bgc = tex.GetPixel (0, 0);
		int x1 = tex.width, y1 = tex.height, x2 = 0, y2 = 0;

		for (int i=0; i<tex.width; i++)
		{
			for (int j=0; j<tex.height; j++)
			{
				Color curColor = tex.GetPixel (i, j);
				// cropping
				if (curColor != bgc)
				{
					if (i < x1)
					{
						x1 = i;
					}
					if (i > x2)
					{
						x2 = i;
					}

					if (j < y1)
					{
						y1 = j;
					}
					if( j > y2)
					{
						y2 = j;
					}
				}
			}
		}
		int width = x2 - x1 +1;
		int height = y2 - y1 +1;
		result = new Texture2D(width, height, TextureFormat.RGB24, false); 

		for (int i=x1; i<=x2; i++)
		{
			for (int j=y1; j<=y2; j++)
			{
				result.SetPixel (i - x1, j - y1, tex.GetPixel (i, j));
			}
		}

		return result;
	}

}
