﻿using UnityEngine;
using System.Collections;

public class Demo : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{

		string fileName = Application.dataPath + "/" + "demo.ini";
		//print(fileName);

		IniFile iniFile = new IniFile();
		iniFile.Load_File(fileName);

		iniFile.Goto_Section("MySection1");
		//print("MySection1.comment = " + iniFile.Get_Section_Comment());
		//print("MySection1.say = " + iniFile.Get_String("say"));
		//print("MySection1.say.comment = " + iniFile.Get_Comment("say"));
		//print("MySection1.player.hp = " + iniFile.Get_Int("player.hp"));
		//print("MySection1.player.hp.comment = " + iniFile.Get_Comment("player.hp"));

		iniFile.Goto_Section("MySection2");
		//print("MySection2.comment = " + iniFile.Get_Section_Comment());
		//print("MySection2.mp = " + iniFile.Get_Float("mp"));

		iniFile.Goto_Section("MySection1");
		iniFile.Set_String("say", "reset!");
		iniFile.Set_Int("player.hp",5,"player hp");

		iniFile.Goto_Section("MySection2");
		iniFile.Set_Float("mp3",1.2f);

		iniFile.Create_Section("NewSection"," this is a new section");
		iniFile.Save();
		iniFile.SaveTo(Application.dataPath + "/" + "demoBackup.ini");


	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
