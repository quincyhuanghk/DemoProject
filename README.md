Command line instructions


Git global setup

git config --global user.name "HaikunHuang"
git config --global user.email "quincy.huanghk@gmail.com"

Create a new repository

git clone git@gitlab.com:HaikunHuang/DemoProject.git
cd DemoProject
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder or Git repository

cd existing_folder
git init
git remote add origin git@gitlab.com:HaikunHuang/DemoProject.git
git add .
git commit
git push -u origin master